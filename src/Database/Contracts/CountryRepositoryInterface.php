<?php

namespace Shopwise\Platform\Database\Contracts;

use Illuminate\Support\Collection;

interface CountryRepositoryInterface
{
    /**
     * Get all of the options from the collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function options(): Collection;
}

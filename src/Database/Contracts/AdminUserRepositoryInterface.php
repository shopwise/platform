<?php

namespace Shopwise\Platform\Database\Contracts;

use Shopwise\Platform\Database\Models\AdminUser;

interface AdminUserRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Find an admin user by its email
     *
     * @param  string $email
     * @return \Shopwise\Platform\Database\Models\AdminUser
     */
    public function findByEmail(string $email): AdminUser;
}

<?php

namespace Shopwise\Platform\Database\Repositories;

use Illuminate\Support\Collection as SupportCollection;
use Shopwise\Platform\Database\Contracts\RoleInterface;
use Shopwise\Platform\Database\Models\Role;

class RoleRepository extends BaseRepository implements RoleInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Role();
    }

    public function model()
    {
        return $this->model;
    }

    public function findAdminRole(): Role
    {
        return Role::whereName(Role::ADMIN)->first();
    }

    public function options(): SupportCollection
    {
        return Role::all()->pluck('name', 'id');
    }
}

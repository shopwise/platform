<?php

namespace Shopwise\Platform\Database\Models;

class Language extends BaseModel
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'is_default'
    ];
}

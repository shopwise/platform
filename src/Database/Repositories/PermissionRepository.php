<?php

namespace Shopwise\Platform\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Shopwise\Platform\Database\Contracts\PermissionInterface;
use Shopwise\Platform\Database\Models\Permission;

class PermissionRepository implements PermissionInterface
{
    public function create(array $data): Permission
    {
        return Permission::create($data);
    }

    public function findByName(string $name)
    {
        return Permission::whereName($name)->first();
    }

    public function find(int $id): Permission
    {
        return Permission::find($id);
    }

    public function all(): Collection
    {
        return Permission::all();
    }
}

<?php

namespace Shopwise\Platform\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Shopwise\Platform\Database\Models\Permission;

interface PermissionRepositoryInterface
{
    /**
     * Create new permission and return its instance
     *
     * @param  array $data
     * @return \Shopwise\Platform\Database\Models\Permission
     */
    public function create(array $data): Permission;

    /**
     * Find a permission by its given name
     *
     * @param  string $name
     * @return \Shopwise\Platform\Database\Models\Permission
     */
    public function findByName(string $name);

    /**
     * Find a permission in the storage
     *
     * @param  int $id
     * @return \Shopwise\Platform\Database\Models\Permission
     */
    public function find(int $id): Permission;

    /**
     * Get all of the permission from the collection
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all(): Collection;
}

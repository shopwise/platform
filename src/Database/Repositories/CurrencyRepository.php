<?php

namespace Shopwise\Platform\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Shopwise\Platform\Database\Contracts\ConfigurationInterface;
use Shopwise\Platform\Database\Contracts\CurrencyInterface;
use Shopwise\Platform\Database\Models\Currency;

class CurrencyRepository extends BaseRepository implements CurrencyInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Currency();
    }

    public function model(): Currency
    {
        return $this->model;
    }

    public function create(array $data): Currency
    {
        return Currency::create($data);
    }

    public function find(int $id): Currency
    {
        return Currency::find($id);
    }

    public function delete(int $id): int
    {
        return Currency::destroy($id);
    }

    public function all(): Collection
    {
        return Currency::all();
    }

    public function getDefault(): Currency
    {
        $configurationRepository = app(ConfigurationInterface::class);
        $id = $configurationRepository->getValueByCode('default_currency');

        return $this->model->find($id);
    }
}

<?php

namespace Shopwise\Platform;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Shopwise\Platform\Commands\InstallCommand;
use Shopwise\Platform\Http\Composers\LayoutComposer;
use Shopwise\Platform\Http\Middleware\AdminAuthenticate;
use Shopwise\Platform\Http\Middleware\Permission;
use Shopwise\Platform\Http\Middleware\RedirectIfAdminAuthenticated;
use Shopwise\Platform\Http\Middleware\ShopwiseCore;

class ShopwiseServiceProvider extends ServiceProvider
{
    /**
     * The mapping of package service providers
     *
     * @var array
     */
    protected $providers = [
        // RepositoryServiceProvider::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerProviders();
        $this->registerConfigs();
        $this->registerRoutes();
        $this->registerMiddlewares();
        $this->registerViewComposers();
        $this->registerCommands();
        $this->registerMigrations();
        $this->registerViews();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootTranslations();
        $this->bootPublishing();
    }

    /**
     * Register the package service providers
     *
     * @return void
     */
    protected function registerProviders()
    {
        //
    }

    /**
     * Register the package configuration
     *
     * @return void
     */
    protected function registerConfigs()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/shopwise.php', 'shopwise');

        $config = include __DIR__.'/../config/shopwise.php';

        // get configurations
        $filesystems = $this->app['config']->get('filesystems', []);
        $auth = $this->app['config']->get('auth', []);

        // set configurations
        $this->app['config']->set('filesystems', array_merge($filesystems, $config['filesystems']));
        $this->app['config']->set('auth.guards', array_merge($auth['guards'], $config['auth']['guards']));
        $this->app['config']->set('auth.providers', array_merge($auth['providers'], $config['auth']['providers']));
        $this->app['config']->set('auth.passwords', array_merge($auth['passwords'], $config['auth']['passwords']));
    }

    /**
     * Register the package routes.
     *
     * @return void
     */
    private function registerRoutes()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
    }

    /**
     * Register the package middlewares
     *
     * @return void
     */
    protected function registerMiddlewares()
    {
        $router = $this->app['router'];
        $router->aliasMiddleware('admin.auth', AdminAuthenticate::class);
        $router->aliasMiddleware('permission', Permission::class);
        $router->aliasMiddleware('admin.guest', RedirectIfAdminAuthenticated::class);
        $router->aliasMiddleware('shopwise', ShopwiseCore::class);
    }

    /**
     * Register the package view composer
     *
     * @return void
     */
    protected function registerViewComposers()
    {
        View::composer('shopwise::layouts.app', LayoutComposer::class);
    }

    /**
     * Register the package console commands
     *
     * @return void
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class
            ]);
        }
    }

    /**
     * Register the package migrations
     *
     * @return void
     */
    protected function registerMigrations()
    {
        $this->loadMigrationsFrom(__DIR__.'/../resources/lang');
    }

    /**
     * Register the package views
     *
     * @return void
     */
    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'shopwise');
    }

    /**
     * Bootstrap the package translations
     *
     * @return void
     */
    protected function bootTranslations()
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'shopwise');
    }

    /**
     * Bootstrap the package publishable resources
     *
     * @return void
     */
    protected function bootPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/shopwise.php' => config_path('shopwise.php'),
            ], 'shopwise-config');

            $this->publishes([
                __DIR__.'/../public' => public_path('vendor/shopwise')
            ], 'shopwise-assets');
        }
    }
}

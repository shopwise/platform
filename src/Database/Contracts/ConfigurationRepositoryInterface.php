<?php

namespace Shopwise\Platform\Database\Contracts;

use Shopwise\Platform\Database\Models\Configuration;

interface ConfigurationRepositoryInterface
{
    public function getValueByCode($code);

    public function getModelByCode($code);

    /**
     * Create a new configuration and return its instance
     *
     * @param  array   $data
     * @return \Shopwise\Platform\Database\Models\Configuration
     */
    public function create(array $data): Configuration;
}

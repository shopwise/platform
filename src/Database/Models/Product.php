<?php

namespace Shopwise\Platform\Database\Models;

class Product extends BaseModel
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'type', 'sku', 'barcode', 'description', 'status', 'in_stock',
        'track_stock', 'qty', 'is_taxable', 'price', 'cost_price', 'weight', 'height',
        'length ', 'meta_title', 'meta_description'
    ];

    const TAX_CONFIG_KEY = 'tax_percentage';
    const PRODUCT_TYPES_BASIC = 'BASIC';
    const PRODUCT_TYPES_DOWNLOADABLE = 'DOWNLOADABLE';
    const PRODUCT_TYPES_VARIABLE_PRODUCT = 'VARIABLE_PRODUCT';

    const PRODUCT_TYPES = [
        self::PRODUCT_TYPES_BASIC => 'Basic',
        self::PRODUCT_TYPES_DOWNLOADABLE => 'Downlodable',
        self::PRODUCT_TYPES_VARIABLE_PRODUCT => 'Variable Product'
    ];
}

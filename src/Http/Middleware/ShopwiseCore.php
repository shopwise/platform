<?php

namespace Shopwise\Platform\Http\Middleware;

use Illuminate\Support\Facades\Session;
use Shopwise\Platform\Database\Contracts\CurrencyRepositoryInterface;

class ShopwiseCore
{
    /**
     * The currency repository instance
     *
     * @var \Shopwise\Platform\Database\Contracts\CurrencyRepositoryInterface
     */
    protected $currencyRepository;

    /**
     * Create a new middleware instance
     *
     * @param  \Shopwise\Platform\Database\Contracts\CurrencyRepositoryInterface $currencyRepository
     * @return void
     */
    public function __construct(CurrencyRepositoryInterface $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * Handle an incoming request
     *
     * @param  \Illuminate\Http\Request   $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $this->setDefaultCurrency();

        return $next($request);
    }

    /**
     * Set the package default currency
     *
     * @return void
     */
    public function setDefaultCurrency()
    {
        if (! Session::has('default_currency')) {
            $currency = $this->currencyRepository->getDefault();
            Session::put('default_currency', $currency);
        }

        if (! Session::has('default_currency_symbol')) {
            $currency = $this->currencyRepository->getDefault();
            Session::put('default_currency_symbol', $currency->symbol);
        }
    }
}

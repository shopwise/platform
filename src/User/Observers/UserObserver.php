<?php

namespace Shopwise\Platform\User\Observers;

use Shopwise\Platform\Database\Contracts\UserGroupRepositoryInterface;

class UserObserver
{
    /**
     * The user group repository
     *
     * @var \Shopwise\Platform\Database\Contracts\UserGroupRepositoryInterface
     */
    protected $userGroupRepository;

    /**
     * Create a new notification instance
     *
     * @param  \Shopwise\Platform\Database\Contracts\UserGroupRepositoryInterface $userGroupRepository
     * @return void
     */
    public function __construct(UserGroupRepositoryInterface $userGroupRepository)
    {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * Handle the user "created" event
     *
     * @param  mixed $user
     * @return void
     */
    public function created($user)
    {
        $userGroup = $this->userGroupRepository->getIsDefault();

        $user->user_group_id = $userGroup->id;
        $user->save();
    }
}

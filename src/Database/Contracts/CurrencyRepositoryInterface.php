<?php

namespace Shopwise\Platform\Database\Contracts;

use Shopwise\Platform\Database\Models\Currency;

interface CurrencyRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Get the default currency from the database
     *
     * @return \Shopwise\Platform\Database\Models\Currency
     */
    public function getDefault(): Currency;
}

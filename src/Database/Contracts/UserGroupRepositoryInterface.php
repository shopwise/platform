<?php

namespace Shopwise\Platform\Database\Contracts;

use Shopwise\Platform\Database\Models\UserGroup;

interface UserGroupRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Get the default user group from the database
     *
     * @return \Shopwise\Platform\Database\Models\UserGroup
     */
    public function getIsDefault(): UserGroup;
}

<?php

namespace Shopwise\Platform\User\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Shopwise\Platform\Database\Contracts\UserGroupRepositoryInterface;
use Shopwise\Platform\Database\Models\UserGroup;
use Shopwise\Platform\User\Requests\UserGroupRequest;

class UserGroupController extends Controller
{
    /**
     * The user group repository instance
     *
     * @var \Shopwise\Platform\Database\Contracts\UserGroupRepositoryInterface
     */
    protected $userGroupRepository;

    /**
     * Create a new controller instance
     *
     * @param  \Shopwise\Platform\Database\Contracts\UserGroupRepositoryInterface $userGroupRepository
     * @return void
     */
    public function __construct(UserGroupRepositoryInterface $userGroupRepository)
    {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $userGroups = $this->userGroupRepository->paginate();

        return view('shopwise::user.user-group.index')
            ->with(compact('userGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Shopwise\Platform\User\Requests\UserGroupRequest $request
     * @return \Illuminate\http\RedirectResponse
     */
    public function store(UserGroupRequest $request)
    {
        $this->userGroupRepository->create($request->all());

        return redirect()
            ->route('admin.user-group.index')
            ->with('successNotification', __('shopwise::system.notification.store', [
                'attribute' => __('shopwise::user.user-group.title')
            ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Shopwise\Platform\Database\Models\UserGroup $userGroup
     * @return \Illuminate\View\View
     */
    public function edit(UserGroup $userGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Shopwise\Platform\User\Requests\UserGroupRequest $request
     * @param  \Shopwise\Platform\Database\Models\UserGroup      $userGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserGroupRequest $request, UserGroup $userGroup)
    {
        if ($request->get('is_default')) {
            $group = $this->userGroupRepository->getIsDefault();
            $group->update(['is_default' => 0]);
        }

        $userGroup->update($request->all());

        return redirect()
            ->route('admin.user-group.index')
            ->with('successNotification', __('shopwise::system.notification.updated', [
                'attribute' => __('shopwise::user.user-group.title')
            ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Shopwise\Platform\Database\Models\UserGroup $userGroup
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(UserGroup $userGroup)
    {
        $userGroup->delete();

        return response()->json([
            'success' => true,
            'message' => __('shopwise::system.notification.delete', [
                'attribute' => __('shopwise::user.user-group.title')
            ])
        ]);
    }
}

<?php

namespace Shopwise\Platform\Database\Contracts;

use Shopwise\Platform\Database\Models\Customer;

interface CustomerRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Find a customer by its given email
     *
     * @param  string $email
     * @return \Shopwise\Platform\Database\Models\Customer
     */
    public function findByEmail(string $email): ?Customer;
}

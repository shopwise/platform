<?php

namespace Shopwise\Platform\Menus;

class MenuItem
{
    const ADMIN = 'admin';

    const FRONT = 'front';

    /**
     * The menu label path
     *
     * @var string
     */
    public $label;

    /**
     * The menu type name
     *
     * @var string
     */
    public $type;

    /**
     * The menu icon name
     *
     * @var string
     */
    public $icon;

    /**
     * The menu attributes
     *
     * @var array
     */
    public $attributes;

    /**
     * The menu key
     *
     * @var string
     */
    public $key;

    /**
     * The menu parameters
     *
     * @var string
     */
    public $parameters;

    /**
     * The menu route path
     *
     * @var string
     */
    public $route;

    /**
     * The menu callback
     *
     * @var callable
     */
    public $callback;

    /**
     * The menu items
     *
     * @var array
     */
    public $items;

    /**
     * Create a new menu instance
     *
     * @param  callable $callable
     * @return void
     */
    public function __construct($callable)
    {
        $this->callback = $callable;
        $callable($this);
    }

    /**
     * Return the menu label
     *
     * @param  string|null $label
     * @return mixed
     */
    public function label($label = null)
    {
        if (null !== $label) {
            $this->label = $label;
        }

        return trans($this->label);
    }

    /**
     * Return the menu type
     *
     * @param  string|null $type
     * @return mixed
     */
    public function type($type = null)
    {
        if (null !== $type) {
            $this->type = $type;
        }

        return $this->type;
    }

    /**
     * Return the menu by its key
     *
     * @param  string|null $key
     * @return string
     */
    public function key($key = null)
    {
        if (null !== $key) {
            $this->key = $key;
        }

        return $this->key;
    }

    /**
     * Return the menu route action
     *
     * @param  string|null $route
     * @return mixed
     */
    public function route($route = null)
    {
        if (null !== $route) {
            $this->route = $route;
        }

        return $this->route;
    }

    /**
     * Return the menu parameters
     *
     * @param  string|null $parameters
     * @return mixed
     */
    public function parameters($parameters = null)
    {
        if (null !== $parameters) {
            $this->parameters = $parameters;
        }

        return $this->parameters;
    }

    /**
     * Return the menu icon
     *
     * @param  string|null $icon
     * @return mixed
     */
    public function icon($icon = null)
    {
        if (null !== $icon) {
            $this->icon = $icon;
        }

        return $this->icon;
    }

    /**
     * Return the menu attributes
     *
     * @param  string|null $attributes
     * @return mixed
     */
    public function attributes($attributes = null)
    {
        if (null !== $attributes) {
            $this->attributes = $attributes;
        }

        return $this->attributes;
    }

    /**
     * Return the sub menu of the item
     *
     * @param  string|null $key
     * @param  mixed       $menu
     * @return mixed
     */
    public function subMenu($key = null, $item = null)
    {
        if (null === $item) {
            return $this->items;
        }

        $menu = new self($item);

        $this->items[$key] = $menu;

        return $this;
    }

    /**
     *  Check if the current item has a sub menu
     *
     * @return bool
     */
    public function hasSubMenu()
    {
        if (isset($this->items) && count($this->items) > 0) {
            return true;
        }

        return false;
    }
}

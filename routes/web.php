<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web'],
    'prefix' => config('shopwise.prefix'),
    'namespace' => 'Shopwise\Platform\User\Controllers',
    'as' => 'admin.'
], function () {
    Route::get('login', 'LoginController@loginForm')->name('login');
});

Route::group([
    'middleware' => ['web', 'admin.auth:admin', 'permission'],
    'prefix' => 'admin',
    'namespace' => 'Shopwise\Platform',
    'as' => 'admin.'
], function () {
    // Route::get('', 'Http\Controllers\DashboardController@index')->name('dashboard');
});

<?php

namespace Shopwise\Platform\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Shopwise\Platform\Database\Contracts\AddressInterface;
use Shopwise\Platform\Database\Models\Address;

class AddressRepository implements AddressInterface
{
    /**
     * Create a new model and return the instance
     *
     * @param  array $data
     * @return \Shopwise\Platform\Database\Models\Address
     */
    public function create(array $data): Address
    {
        return Address::create($data);
    }

    /**
     * Find a model by its key
     *
     * @param  int $id
     * @return \Shopwise\Platform\Database\Models\Address
     */
    public function find(int $id): Address
    {
        return Address::find($id);
    }

    /**
     * Delete a record from the database by its key
     *
     * @param  int $id
     * @return int
     */
    public function delete(int $id): int
    {
        return Address::destroy($id);
    }

    /**
     * Return all of the items from the collection
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all(): Collection
    {
        return Address::all();
    }

    /**
     * Get a customer from the model by its id
     *
     * @param  int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getByCustomerId(int $userId): Collection
    {
        return Address::with('country')->whereCustomerId($userId)->get();
    }
}

<?php

namespace Shopwise\Platform\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface OrderRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Find an order of a given customer id
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByCustomerId(int $id): Collection;

    /**
     * Get the current month total orders
     *
     * @return int
     */
    public function getCurrentMonthTotalOrder(): int;

    /**
     * Get the current month total revenue
     *
     * @return float
     */
    public function getCurrentMonthTotalRevenue(): float;
}

<?php

namespace Shopwise\Platform\Database\Repositories;

use Shopwise\Platform\Database\Contracts\UserGroupInterface;
use Shopwise\Platform\Database\Models\UserGroup;

class UserGroupRepository extends BaseRepository implements UserGroupInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new UserGroup();
    }

    public function model(): UserGroup
    {
        return $this->model;
    }

    public function getIsDefault(): UserGroup
    {
        return UserGroup::whereIsDefault(true)->first();
    }
}

<?php

namespace Shopwise\Platform\Database\Models;

class Role extends BaseModel
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    const ADMIN = 'Administrator';

    public function hasPermission($routes)
    {
        $modelPermissions = $this->permissions->pluck('name');
        $permissions = explode(',', $routes);
        $hasPermission = true;

        foreach ($permissions as $permissions) {
            if (! $modelPermissions->contains($permissions)) {
                $hasPermission = false;
            }
        }

        return $hasPermission;
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}

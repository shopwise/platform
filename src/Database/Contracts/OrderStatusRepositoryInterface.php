<?php

namespace Shopwise\Platform\Database\Contracts;

use Shopwise\Platform\Database\Models\OrderStatus;

interface OrderStatusRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Find the default status of the order
     *
     * @return \Shopwise\Platform\Database\Models\OrderStatus
     */
    public function findDefault(): OrderStatus;
}

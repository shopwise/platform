<?php

namespace Shopwise\Platform\Database\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

interface BaseRepositoryInterface
{
    /**
     * Paginate the given query into a paginator
     *
     * @param  int                                                   $perPage
     * @param  array                                                 $with
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 10, array $with = []): LengthAwarePaginator;

    /**
     * Begin querying the model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(): Builder;

    /**
     * Create a new instance of the related model
     *
     * @param  array $data
     * @return \Shopwise\Platform\Database\Models\BaseModel
     */
    public function create(array $data);

    /**
     * Find a model by its key
     *
     * @param  int  $id
     * @return mixed
     */
    public function find(int $id);

    /**
     * Delete a model record from the database
     *
     * @param  int $id
     * @return int
     */
    public function delete(int $id): int;

    /**
     * Get all of the items from the collection
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all(): Collection;
}

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        >meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('meta_title', 'Shopwise eCommerce')</title>

        <!-- Styles -->
        @if (file_exists(public_path('mix-manifest.json')))
            <link href="{{ mix('vendor/shopwise/css/app.css') }}" rel="stylesheet">
        @else
            <link href="{{ asset('vendor/shopwise/css/app.css') }}" rel="stylesheet">
        @endif
    </head>
    <body>
        <div id="app">
            <Shopwise-Layout inline-template>
                {{-- start here --}}
            </Shopwise-Layout>
        </div>

        @if (file_exists(public_path('mix-manifest.json')))
            <script src="{{ mix('vendor/shopwise/js/shopwise.js') }}"></script>
        @else
            <script src="{{ asset('vendor/shopwise/js/shopwise.js') }}"></script>
        @endif

        @stack('scripts')

        @if (file_exists(public_path('mix-manifest.json')))
            <script src="{{ mix('vendor/shopwise/js/app.js') }}"></script>
        @else
            <script src="{{ asset('vendor/shopwise/js/app.js') }}"></script>
        @endif
    </body>
</html>

<?php

namespace Shopwise\Platform\Database\Repositories;

use Shopwise\Platform\Database\Contracts\ConfigurationInterface;
use Shopwise\Platform\Database\Models\Configuration;

class ConfigurationRepository implements ConfigurationInterface
{
    public function getValueByCode($code)
    {
        $configuration = Configuration::whereCode($code)->first();
        if ($configuration === null) {
            return;
        }

        return $configuration->value;
    }

    public function getModelByCode($code)
    {
        $configuration = Configuration::whereCode($code)->first();
        if ($configuration === null) {
            return;
        }

        return $configuration;
    }

    public function create(array $data): Configuration
    {
        return Configuration::create($data);
    }
}

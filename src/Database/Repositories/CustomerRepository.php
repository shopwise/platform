<?php

namespace Shopwise\Platform\Database\Repositories;

use Shopwise\Platform\Database\Contracts\CustomerInterface;
use Shopwise\Platform\Database\Models\Customer;

class CustomerRepository extends BaseRepository implements CustomerInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Customer();
    }

    public function model(): Customer
    {
        return $this->model;
    }

    public function findByEmail(string $email): ?Customer
    {
        return Customer::whereEmail($email)->first();
    }
}

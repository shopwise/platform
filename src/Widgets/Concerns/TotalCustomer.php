<?php

namespace Shopwise\Platform\Widgets\Concerns;

use Illuminate\Support\Carbon;

class TotalCustomer
{
    /**
     * The widget view path
     *
     * @var string
     */
    protected $view = "shopwise::widget.total-customer";

    /**
     * The widget label
     *
     * @var string
     */
    protected $label = 'Total Customer';

    /**
     * The widget type
     *
     * @var string
     */
    protected $type = 'cms';

    /**
     * The widget identifier
     *
     * @var string
     */
    protected $identifier = 'shopwise-total-customer';

    /**
     * Get the evaluated view contents of a view
     *
     * @return string
     */
    public function view()
    {
        return $this->view;
    }

    /**
     * Get the identifier of the widget
     *
     * @return string
     */
    public function identifier()
    {
        return $this->identifier;
    }

    /**
     * Get the label of the widget
     *
     * @return string
     */
    public function label()
    {
        return $this->label;
    }

    /**
     * Get the type of the widget
     *
     * @return string
     */
    public function type()
    {
        return $this->type;
    }

    /**
     * Add a piece of data to the view
     *
     * @return mixed
     */
    public function with()
    {
        $userModel = $this->getUserModel();
        $firstDay = $this->getFirstDay();

        if ($userModel === null) {
            $value = 0;
        } else {
            $userModel->select('id')->where('created_at', '>', $firstDay)->count();
        }

        return ['value' => $value];
    }

    /**
     * Get the string contents of the view
     *
     * @return string
     */
    public function render()
    {
        return view($this->view(), $this->with());
    }

    /**
     * Get the starting day of the month
     *
     * @return \Illuminate\Support\Carbon
     */
    protected function getFirstDay()
    {
        $startDay = Carbon::now();

        return $startDay->firstOfMonth();
    }

    protected function getUserModel()
    {
        $user = config('shopwise.model.user');

        try {
            $model = app($user);
        } catch (\Exception $e) {
            $model = null;
        }

        return $model;
    }
}

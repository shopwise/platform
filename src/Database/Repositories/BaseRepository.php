<?php

namespace Shopwise\Platform\Database\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository
{
    abstract function model();

    /**
     * Paginate the given query into a paginator
     *
     * @param  int  $perPage
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 10): LengthAwarePaginator
    {
        return $this->model()->paginate($perPage);
    }

    /**
     * Begin querying the model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(): Builder
    {
        return $this->model()->query();
    }

    /**
     * Create a new instance of the related model
     *
     * @param  array $data
     * @return \Shopwise\Platform\Database\Models\BaseModel
     */
    public function create(array $data)
    {
        return $this->model()->create($data);
    }

    /**
     * Find a model resource  by its id
     *
     * @param  int  $id
     * @return \Shopwise\Platform\Database\Models\BaseModel
     */
    public function find(int $id)
    {
        return $this->model()->find($id);
    }

    /**
     * Delete a model resource in the storage by id
     *
     * @param  int $id
     * @return int
     */
    public function delete(int $id): int
    {
        return $this->model()->delete($id);
    }

    /**
     * Get all model collection in the storage
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all(): Collection
    {
        return $this->model()->all();
    }
}

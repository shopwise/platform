<?php

namespace Shopwise\Platform\Widgets;

use Illuminate\Support\Collection;

class WidgetManager
{
    /**
     * The collection instance
     *
     * @var \Illuminate\Support\Collection
     */
    protected $collection;

    /**
     * Create new widget manager instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = Collection::make([]);
    }

    /**
     * Create a new widget instance
     *
     * @param  [type] $key
     * @param  [type] $widget
     * @return void
     */
    public function make($key, $widget)
    {
        $this->collection->put($key, $widget);

        return $widget;
    }

    /**
     * Get a widget from the collection by key
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->collection->get($key);
    }

    /**
     * Get all of the widget from the collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return $this->collection;
    }

    /**
     * Returns all of the widgets in the collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function options(): Collection
    {
        $options = Collection::make([]);

        foreach ($this->collection as $key => $widget) {
            $options->put($key, $widget->label());
        }

        return $options;
    }
}

<?php

namespace Shopwise\Platform\Database\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;
use Shopwise\Platform\Database\Contracts\OrderInterface;
use Shopwise\Platform\Database\Models\Order;

class OrderRepository extends BaseRepository implements OrderInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Order();
    }

    public function model(): Order
    {
        return $this->model;
    }

    public function findByCustomerId(int $id): Collection
    {
        return Order::whereCustomerId($id)->get();
    }

    public function getCurrentMonthTotalOrder() : int
    {
        $firstDay = $this->getFirstDay();
        $totalOrder = Order::select('id')->where('created_at', '>', $firstDay)->count();

        return $totalOrder;
    }

    public function getCurrentMonthTotalRevenue() : float
    {
        $total = 0;
        $firstDay = $this->getFirstDay();
        $orders = Order::with('products')
            ->select('*')
            ->where('created_at', '>', $firstDay)
            ->get();

        foreach ($orders as $order) {
            foreach ($order->products as $product) {
                $total += ($product->qty * $product->price) + $product->tax_amount;
            }
        }

        return $total;
    }

    private function getFirstDay()
    {
        $startDay = Carbon::now();

        return $startDay->firstOfMonth();
    }
}

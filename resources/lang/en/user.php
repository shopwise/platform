<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple user links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'notification' => [
        'store' => '',
        'updated' => '',
        'delete' => '',
        'upload' => ''
    ],

    'previous' => '&laquo; Previous',
    'next' => 'Next &raquo;',

];

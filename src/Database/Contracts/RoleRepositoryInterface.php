<?php

namespace Shopwise\Platform\Database\Contracts;

use Illuminate\Support\Collection as SupportCollection;
use Shopwise\Platform\Database\Models\Role;

interface RoleRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Find an admin role from the storage
     *
     * @return \Shopwise\Platform\Database\Models\Role
     */
    public function findAdminRole(): Role;

    /**
     * Get all of the options from the collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function options(): SupportCollection;
}

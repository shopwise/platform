<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Route Name Prefix
    |--------------------------------------------------------------------------
    |
    | This may be used to prefix each route name in the group with a given string.
    | For example, you may want to prefix all of the group routes names with `admin`
    |
    */

    'prefix' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | User Model Class
    |--------------------------------------------------------------------------
    |
    | This configuration option specify a model class to use for user storage
    | to the database.
    |
    */

    'model' => [
        'user' => \Shopwise\Platform\Database\Models\Customer::class
    ],

    /*
    |--------------------------------------------------------------------------
    | Filesystems
    |--------------------------------------------------------------------------
    |
    | Here, you may specify the default filesystem disks that should be used
    | by Sentrio. The "local" disk, as well as variety of cloud based disks are
    | available to you.
    |
    */

    'filesystems' => [
        'disks' => [
            'shopwise' => [
                'driver' => 'local',
                'root' => storage_path('app/public')
            ]
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider that defines how the
    | users are actually retrieved out of the database or other storage
    | mechanisms used by the application to persist your users data.
    */

    'auth' => [

        /*
        |--------------------------------------------------------------------------
        | Authentication Guards
        |--------------------------------------------------------------------------
        |
        | You may define every authentication guard for your application. The
        | configuration uses session storage and the Eloquent user provider.
        |
        */

        'guards' => [
            'admin' => [
                'driver' => 'session',
                'provider' => 'admin-users'
            ],

            'admin_api' => [
                'driver' => 'passport',
                'provider' => 'admin-users',
                'hash' => false
            ],

            'customer' => [
                'driver' => 'session',
                'provider' => 'customers'
            ]
        ],

        /*
        |--------------------------------------------------------------------------
        | User Providers
        |--------------------------------------------------------------------------
        |
        | All authentication drivers have a user provider. That defines how the users
        | are actually retrieved out of the database or other storage mechanisms used
        | by this platform to persist your users data
        |
        */

        'providers' => [
            'admin-users' => [
                'driver' => 'eloquent',
                'model' => \Shopwise\Platform\Database\Models\AdminUser::class
            ],

            'customers' => [
                'driver' => 'eloquent',
                'model' => \Shopwise\Platform\Database\Models\Customer::class
            ]
        ],

        /*
        |--------------------------------------------------------------------------
        | Resetting Passwords
        |--------------------------------------------------------------------------
        |
        | You may specify multiple password reset configurations if you have more
        | than one user table or model in the application and you want to have
        | separate password reset settings based on the specific user type.
        |
        | The expire time is the number of minutes that each reset token will be
        | considered valid.
        |
        */

        'passwords' => [
            'adminusers' => [
                'provider' => 'admin-users',
                'table' => 'admin_password_resets',
                'expire' => 60
            ]
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Image
    |--------------------------------------------------------------------------
    |
    |
    */

    // 'image' => []

];

<?php

namespace Shopwise\Platform\User\Controllers;

use Illuminate\Routing\Controller;
use Shopwise\Platform\Database\Contracts\AdminUserRepositoryInterface;
use Shopwise\Platform\Database\Contracts\RoleRepositoryInterface;
use Shopwise\Platform\Database\Models\AdminUser;
use Shopwise\Platform\User\Requests\AdminUserImageRequest;
use Shopwise\Platform\User\Requests\AdminUserRequest;

class AdminUserController extends Controller
{
    /**
     * The admin user repository instance
     *
     * @var \Shopwise\Platform\Database\Contracts\AdminUserRepositoryInterface
     */
    protected $adminUserRepository;

    /**
     * The role repository instance
     *
     * @var \Shopwise\Platform\Database\Contracts\RoleRepositoryInterface
     */
    protected $roleRepository;

    /**
     * Create a new controller instance
     *
     * @param  \Shopwise\Platform\Database\Contracts\AdminUserRepositoryInterface $adminUserRepository
     * @param  \Shopwise\Platform\Database\Contracts\RoleRepositoryInterface      $roleRepository
     * @return void
     */
    public function __construct(AdminUserRepositoryInterface $adminUserRepository, RoleRepositoryInterface $roleRepository)
    {
        $this->adminUserRepository = $adminUserRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $adminUsers = $this->adminUserRepository->paginate();

        return view('shopwise::user.admin-user.index')
            ->with(compact('adminUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Shopwise\Platform\User\Requests\AdminUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminUserRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);

        $this->adminUserRepository->create($request->all());

        return redirect()
            ->route('admin.admin-user.index')
            ->with('successNotification', __('shopwise::user.notification.store', ['attribute' => 'AdminUser']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Shopwise\Platform\Database\Models\AdminUser $adminUser
     * @return \Illuminate\View\View
     */
    public function edit(AdminUser $adminUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Shopwise\Platform\User\Requests\AdminUserRequest $request
     * @param  \Shopwise\Platform\Database\Models\AdminUser      $adminUser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminUserRequest $request, AdminUser $adminUser)
    {
        $adminUser->update($request->all());

        return redirect()
            ->route('admin.admin-user.index')
            ->with('successNotification', __('shopwise::user.notification.updated', ['attribute' => 'AdminUser']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Shopwise\Platform\Database\Models\AdminUser $adminUser
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(AdminUser $adminUser)
    {
        $adminUser->delete();

        return response()->json([
            'success' => true,
            'message' => __('shopwise::user.notification.delete', ['attribute' => 'AdminUser'])
        ]);
    }

    /**
     * Upload a specified user image in resource.
     *
     * @param  \Shopwise\Platform\User\Requests\AdminUserImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(AdminUserImageRequest $request)
    {
        $image = $request->file('files');
        $path = $image->store('uploads/users', 'shopwise');

        return response()->json([
            'success' => true,
            'path' => $path,
            'message' => __('shopwise::user.notification.upload', ['attribute' => 'Admin User Image'])
        ]);
    }
}

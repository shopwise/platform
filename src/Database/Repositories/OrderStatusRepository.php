<?php

namespace Shopwise\Platform\Database\Repositories;

use Shopwise\Platform\Database\Contracts\OrderStatusInterface;
use Shopwise\Platform\Database\Models\OrderStatus;

class OrderStatusRepository extends BaseRepository implements OrderStatusInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new OrderStatus();
    }

    public function model(): OrderStatus
    {
        return $this->model;
    }

    public function findDefault(): OrderStatus
    {
        return OrderStatus::whereIsDefault(1)->first();
    }
}

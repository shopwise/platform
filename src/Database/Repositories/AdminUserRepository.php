<?php

namespace Shopwise\Platform\Database\Repositories;

use Shopwise\Platform\Database\Contracts\AdminUserInterface;
use Shopwise\Platform\Database\Models\AdminUser;

class AdminUserRepository extends BaseRepository implements AdminUserInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new AdminUser();
    }

    public function model(): AdminUser
    {
        return $this->model;
    }

    public function findByEmail(string $email): AdminUser
    {
        return AdminUser::whereEmail($email)->first();
    }
}

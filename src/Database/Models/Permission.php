<?php

namespace Shopwise\Platform\Database\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function roles()
    {
        return $this->hasMany(Role::class);
    }
}

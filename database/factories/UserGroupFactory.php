<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Shopwise\Platform\Database\Models\UserGroup;

$factory->define(UserGroup::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'is_default' => rand(0, 1)
    ];
});

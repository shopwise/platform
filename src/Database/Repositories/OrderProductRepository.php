<?php

namespace Shopwise\Platform\Database\Repositories;

use Shopwise\Platform\Database\Contracts\OrderProductInterface;
use Shopwise\Platform\Database\Models\OrderProduct;
use Shopwise\Platform\Order\Events\OrderProductCreated;

class OrderProductRepository extends BaseRepository implements OrderProductInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new OrderProduct();
    }

    public function model(): OrderProduct
    {
        return $this->model;
    }

    public function create(array $data): OrderProduct
    {
        $orderProduct = parent::create($data);
        event(new OrderProductCreated($orderProduct));

        return $orderProduct;
    }
}

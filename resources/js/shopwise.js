window.Vue = require("vue");

window.Shopwise = (function () {
    return {
        initialize: function (callback) {
            callback(window.Vue);
        }
    };
})();

window.EventBus = new Vue()

exports = module.exports = Shopwise;

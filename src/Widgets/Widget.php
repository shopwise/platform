<?php

namespace Shopwise\Platform\Widgets;

class Widget
{
    /**
     * The label of widget
     *
     * @var string
     */
    public $label = null;

    /**
     * The type of widget
     *
     * @var string
     */
    public $type = null;

    /**
     * The callable widget instance
     *
     * @var callable
     */
    protected $callable = null;

    /**
     * Create a new widget instance
     *
     * @param  callable $callable
     * @return void
     */
    public function __construct($callable)
    {
        $this->callable = $callable;
        $callable($this);
    }

    /**
     * The widget label
     *
     * @param  string|null $label
     * @return mixed
     */
    public function label($label = null)
    {
        if (null === $label) {
            return $this->label;
        }

        return $this->label = $label;
    }

    /**
     * The widget type
     *
     * @param  string|null $type
     * @return mixed
     */
    public function type($type = null)
    {
        if (null === $type) {
            return $this->type;
        }

        return $this->type = $type;
    }
}

<?php

namespace Shopwise\Platform\Database\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;
use Shopwise\Platform\Database\Models\Product;

interface ProductRepositoryInterface
{
    /**
     * Get all of the products from the storage
     *
     * @param  int $perPage
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getAllWithoutVariation(int $perPage): LengthAwarePaginator;

    /**
     * Find a product by its slug
     *
     * @param  string $slug
     * @return \Shopwise\Platform\Database\Models\Product
     */
    public function findBySlug(string $slug): Product;

    /**
     * Find a product by its barcode
     *
     * @param  string $barcode
     * @return \Shopwise\Platform\Database\Models\Product
     */
    public function findByBarcode(string $barcode): Product;
}

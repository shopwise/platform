<?php

namespace Shopwise\Platform\Database\Models;

class UserGroup extends BaseModel
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name', 'is_default'
    ];
}

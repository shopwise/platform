<?php

namespace Shopwise\Platform\Tests;

use Orchestra\Testbench\TestCase as OrchestraTestCase;
use Shopwise\Platform\ShopwiseServiceProvider;

class TestCase extends OrchestraTestCase
{
    public function setup() : void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
        $this->artisan('migrate', ['--database' => 'testing']);

        $this->loadMigrationsFrom(__DIR__ . '/../src/database/migrations');
        $this->loadLaravelMigrations(['--database' => 'testing']);

        $this->withFactories(__DIR__.'/../src/database/factories');
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('app.key', 'AckfSECXIvnK5r28GVIWUAxmbBSjTsmF');
    }

    protected function getPackageProviders($app)
    {
        return [
            ShopwiseServiceProvider::class
        ];
    }
}

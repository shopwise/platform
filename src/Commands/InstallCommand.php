<?php

namespace Shopwise\Platform\Commands;

use Illuminate\Console\Command;
use Shopwise\Platform\ShopwiseServiceProvider;

class InstallCommand extends Command
{
    /**
     * The progress bar instance
     *
     * @var \Symfony\Component\Console\Helper\ProgressBar
     */
    protected $progressBar;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopwise:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the Shopwise e-commerce platform';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->progressBar = $this->output->createProgressBar(3);
        $this->introMessage();
        sleep(1);

        $this->info('Installation of Shopwise package, publish assets and configuration files');

        if (! $this->progressBar->getProgress()) {
            $this->progressBar->start();
        }

        $this->comment('Publishing Shopwise Service Provider...');
        $this->callSilent('vendor:publish', ['--provider' => ShopwiseServiceProvider::class]);

        $this->progressBar->advance();

        $this->setupDatabase();
    }

    protected function setupDatabase()
    {
        $this->info('Dropping all the tables in database...');
        $this->call('migrate:fresh');
        $this->progressBar->advance();

        // visually slow down the installation process so that the user can read what's happening
        usleep(350000);
    }

    protected function introMessage()
    {
        $this->info('

               ======================== Starting  Installation! ======================
        ');
    }

    protected function outroMessage()
    {
        $this->progressBar->finish();

        $this->info('

               ======================== Installation Complete! ======================
        ');

        $this->comment('To create an admin user, run `php artisan shopwise:admin`');
    }
}

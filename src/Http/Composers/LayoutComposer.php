<?php

namespace Shopwise\Platform\Http\Composers;

use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class LayoutComposer
{
    /**
     * Bind the data to the view
     *
     * @param  \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        $routeName = Route::currentRouteName();
    }
}

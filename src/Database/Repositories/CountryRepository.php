<?php

namespace Shopwise\Platform\Database\Repositories;

use Illuminate\Support\Collection;
use Shopwise\Platform\Database\Contracts\CountryInterface;
use Shopwise\Platform\Database\Models\Country;

class CountryRepository implements CountryInterface
{
    public function options(): Collection
    {
        return Country::all()->pluck('name', 'id');
    }

    public function currencyCodeOptions(): Collection
    {
        return Country::alll()->pluck('currency_code', 'currency_code');
    }

    public function currencySymbolOptions(): Collection
    {
        return Country::all()->pluck('currency_symbol')->unique();
    }
}

<?php

namespace Shopwise\Platform\Database\Repositories;

use Shopwise\Platform\Database\Contracts\LanguageInterface;
use Shopwise\Platform\Database\Models\Language;

class LanguageRepository extends BaseRepository implements LanguageInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = new Language();
    }

    public function model(): Language
    {
        return $this->model;
    }
}

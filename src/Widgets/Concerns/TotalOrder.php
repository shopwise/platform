<?php

namespace Shopwise\Platform\Widgets\Concerns;

class TotalOrder
{
    /**
     * The widget view path
     *
     * @var string
     */
    protected $view = "shopwise::widget.total-order";

    /**
     * The widget label
     *
     * @var string
     */
    protected $label = 'Total Order';

    /**
     * The widget type
     *
     * @var string
     */
    protected $type = 'cms';

    /**
     * The widget identifier
     *
     * @var string
     */
    protected $identifier = 'shopwise-total-order';

    /**
     * Get the evaluated view contents of a view
     *
     * @return string
     */
    public function view()
    {
        return $this->view;
    }

    /**
     * Get the identifier of the widget
     *
     * @return string
     */
    public function identifier()
    {
        return $this->identifier;
    }

    /**
     * Get the label of the widget
     *
     * @return string
     */
    public function label()
    {
        return $this->label;
    }

    /**
     * Get the type of the widget
     *
     * @return string
     */
    public function type()
    {
        return $this->type;
    }

    /**
     * Add a piece of data to the view
     *
     * @return mixed
     */
    public function with()
    {
        //
    }

    /**
     * Get the string contents of the view
     *
     * @return string
     */
    public function render()
    {
        return view($this->view(), $this->with());
    }
}

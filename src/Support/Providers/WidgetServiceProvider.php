<?php

namespace Shopwise\Platform\Support\Providers;

use Illuminate\Support\ServiceProvider;
use Shopwise\Platform\Support\Facades\Widget;
use Shopwise\Platform\Widgets\Concerns\TotalCustomer;
use Shopwise\Platform\Widgets\Concerns\TotalOrder;
use Shopwise\Platform\Widgets\Concerns\TotalRevenue;
use Shopwise\Platform\Widgets\WidgetManager;

class WidgetServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('widget', function ($app) {
            return new WidgetManager;
        });

        $this->app->alias('widget', 'Shopwise\Platform\Widgets\WidgetManager');
    }

    /**
     * Bootstrap any application services
     *
     * @return void
     */
    public function boot()
    {
        $totalCustomer = new TotalCustomer;
        $totalOrder = new TotalOrder;
        $totalRevenue = new TotalRevenue;

        Widget::make($totalCustomer->identifier(), $totalCustomer);
        Widget::make($totalOrder->identifier(), $totalOrder);
        Widget::make($totalRevenue->identifier(), $totalOrder);
    }

    /**
     * Get the services provided by the provider
     *
     * @return array
     */
    public function provides()
    {
        return ['widget', 'Shopwise\Platform\Widgets\WidgetManager'];
    }
}

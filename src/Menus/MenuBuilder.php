<?php

namespace Shopwise\Platform\Menus;

use Illuminate\Support\Collection;

class MenuBuilder
{
    /**
     * The menu builder instance collection
     *
     * @var \Illuminate\Support\Collection
     */
    protected $collection;

    /**
     * Create a new menu builder instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = Collection::make([]);
    }

    public function make($key, callable $callable)
    {
        $menu = new MenuItem($callable);
        $menu->key($key);

        $this->collection->put($key, $menu);

        return $this;
    }

    public function get($key)
    {
        return $this->collection->get($key);
    }

    public function all($isAdmin = false)
    {
        if ($isAdmin) {
            return $this->collection->filter(function ($menu) {
                return $menu->type() === MenuItem::ADMIN;
            });
        } else {
            return $this->collection->filter(function ($menu) {
                return $menu->type() === MenuItem::FRONT;
            });
        }
    }

    public function getMenuItemFromRouteName($name)
    {
        $currentOpenKey = '';
        $currentMenuItemKey = '';

        foreach ($this->collection as $key => $menuGroup) {
            if ($menuGroup->hasSubMenu()) {
                $subMenus = $menuGroup->subMenu($key);
                foreach ($subMenus as $subKey => $subMenu) {
                    if ($subMenu->route() == $name) {
                        $currentOpenKey = $key;
                        $currentMenuItemKey = $subMenu->key();
                    }
                }
            }
        }

        return [$currentOpenKey, $currentMenuItemKey];
    }

    public function frontMenus()
    {
        $frontMenus = collect();
        $i = 1;

        foreach ($this->collection as $item) {
            if ($item->type() === MenuItem::FRONT) {
                $menu = new \stdClass;
                $menu->id = $i;
                $menu->name = $item->label;
                $menu->url = route($item->route(), $item->parameters());
                $menu->subMenu = $item->subMenu ?? [];

                $frontMenus->push($menu);
                $i++;
            }
        }

        return $frontMenus;
    }

    public function adminMenus()
    {
        $adminMenus = $this->all(true);

        $result = $adminMenus->map(function ($item, $index) {
            $routeName = $item->route();

            if ($item->hasSubMenu()) {
                $subMenus = collect($this->subMenu)->map(function ($item) {
                    $routeName = $item->route();

                    return [
                        'name' => $item->label(),
                        'url' => $routeName === '#' ? '#' : route($routeName, $item->parameters())
                    ];
                });
            }

            return [
                'name' => $item->label(),
                'icon' => $item->icon(),
                'url' => $routeName === '#' ? '#' : route($routeName, $item->parameters()),
                'subMenus' => $subMenus
            ];
        });

        return $result;
    }
}
